
In this R file you can have a look at predictions concerning bladder cancer data from bladderbatch package. We created different classifiers :

- Multiclasspairs

- Random Forest

- PamR

- Random Forest SRC 

After creating them, we used them on test data for predictions. We also visualized confusion matrices, heatmaps representing top k genes (the most differentially expressed ones). In the end, we stored the classifiers results in different tabs (accuracy, kappa, etc). In addition to the R file, you can have a look at the HTML output created with Rmarkdown.

Have fun !

Author : Marion Estoup

Mail : marion_110@hotmail.fr

Date : June 2023